#!/usr/bin/python3
from tabulate import tabulate
import numpy as np
from numpy import random
import matplotlib.pyplot as plt

# ------------------------#
# -------| Outils |-------#
# ------------------------#

epsilon = 1e-4


def agv_lgth(cwd, proba):
    lgth = 0
    for i in range(len(cwd)):
        lgth += len(cwd[i])*proba[i]
    return lgth


def entropie(fMasses):
    epsilon = 1e-15
    entropie = 0
    for i in range(len(fMasses)):
        if fMasses[i] > epsilon:
            entropie += fMasses[i]*np.log2(fMasses[i])
    return -entropie


def somme_kraft(cwd):
    somme = 0
    for c in cwd:
        somme += 2**(-len(c))
    return somme

# ------------------------#
# --------| Noeud |-------#
# ------------------------#


class Node:
    def __init__(self, l=None, r=None, i=None, p=0):
        self.left = l
        self.right = r
        self.prob = p
        self.key = i


def is_leave(tree):
    return tree.right == None and tree.left == None


def node_tri_bulle(NodeTab):
    i = len(NodeTab)-1
    while (NodeTab[i].prob >= NodeTab[i-1].prob and i > 0):
        NodeTmp = NodeTab[i-1]
        NodeTab[i-1] = NodeTab[i]
        NodeTab[i] = NodeTmp
        i -= 1
    return NodeTab

# ------------------------#
# ------| Encodage |------#
# ------------------------#


def huffman_tree(probas):
    NodeTab = [Node(p=probas[key], i=key) for key in range(len(probas))]
    while len(NodeTab) > 1:
        NodeTmp = Node(l=NodeTab[-2], r=NodeTab[-1],
                       p=NodeTab[-2].prob+NodeTab[-1].prob)
        NodeTab = NodeTab[:-2]
        NodeTab.append(NodeTmp)
        NodeTab = node_tri_bulle(NodeTab)
    return NodeTab[0]


def get_cwd(tree):
    cwd = []
    queue = []
    if not (is_leave(tree)):
        queue.append(('', tree))
    while len(queue) > 0:
        node = queue.pop(0)
        if is_leave(node[1]):
            cwd.append(node[0])
        else:
            queue.append((node[0]+'0', node[1].left))
            queue.append((node[0]+'1', node[1].right))
    return cwd


def huffman_code(probas):
    tree = huffman_tree(probas)
    cwd = get_cwd(tree)
    lgth = agv_lgth(cwd, probas)
    return cwd, lgth

# ------------------------#
# ------| Décodage |------#
# ------------------------#


def huffman_tree2(cwd):
    tree = Node()
    for i in range(len(cwd)):
        node = tree
        for c in cwd[i]:
            if c == '0':
                if node.left == None:
                    node.left = Node()
                node = node.left
            else:
                if node.right == None:
                    node.right = Node()
                node = node.right
        node.key = i
    return tree


def cwd_detect(tree, seq):
    node = tree
    for i in range(len(seq)):
        if is_leave(node):
            return (i, node.key)
        elif seq[i] == '1':
            node = node.right
        else:
            node = node.left
    return (len(seq), node.key)


def huffman_decode(seq, symb, cwd):
    tree = huffman_tree2(cwd)
    msg = []
    i = 0
    while (i < len(seq)):
        out = cwd_detect(tree, seq[i:])
        msg.append(symb[out[1]])
        i += out[0]
    return msg
