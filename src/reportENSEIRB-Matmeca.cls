%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      Setup du document      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Utilisation de la classe "report" pour le document
\LoadClass[10pt,a4paper,twoside]{report}

%Configuration de l'encodage et des polices du document
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{lmodern}
\renewcommand{\familydefault}{\sfdefault}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Options du document     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareOption{fr}{
    \newcommand{\docLanguage}{french}
    \newcommand{\tableOfContentName}{Table des matières}
}
\DeclareOption{en}{
    \newcommand{\docLanguage}{english}
    \newcommand{\tableOfContentName}{Table of content}
}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Importation des paquets   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Configuration de la mise en forme du document
\RequirePackage[left=2.5cm,right=2.5cm,top=2cm,bottom=3.5cm]{geometry}
\RequirePackage{xspace}
\RequirePackage{titlesec}
\RequirePackage{fancyhdr}
\RequirePackage{subcaption}

% Configuration des couleurs
\RequirePackage[table]{xcolor}
\RequirePackage{transparent}
\RequirePackage{colortbl}
\RequirePackage{color}

% ENSPIMA
\definecolor{darkblue}{HTML}{1C3B75}
% ENSEIRB-Matmeca
\definecolor{lightblue}{HTML}{1C3B75}
% ENSEGID
\definecolor{green}{HTML}{2A7B6F}
% ENSTBB
\definecolor{orange}{HTML}{E7570F}
% ENSMAC
\definecolor{red}{HTML}{94241A}
% ENSC
\definecolor{magenta}{HTML}{70102F}

% Gestion des tableaux
\RequirePackage{booktabs}
\RequirePackage{multicol}
\RequirePackage{multirow}
\RequirePackage[\docLanguage]{hyperref}
\newcolumntype{M}[1]{>{\centering}m{#1}}
\def\listingautorefname{listing}

% Gestion des figures et des images
\RequirePackage{wrapfig}
\RequirePackage[export]{adjustbox}
\RequirePackage{graphicx}
\RequirePackage{subcaption}
\RequirePackage{wallpaper}

% Gestions des écritures mathématiques et des graphes
\RequirePackage{amsmath,amssymb,amsthm,amsfonts}

% Configuration des langues
\RequirePackage[\docLanguage]{babel}
\RequirePackage{csquotes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Modifications de mise en forme   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Redéfinition du titre de chapitre
\addto\captionsfrench{\def\partname{}}
\definecolor{gray75}{gray}{0.75}
\makeatletter
\def\@makechapterhead#1{%
    {\parindent \z@ \Huge\bfseries
            \interlinepenalty\@M
            \thechapter\hsp\textcolor{gray75}{|}\hsp \Huge\bfseries #1 \hfill
            \vskip -15pt
            \HRule
            \vskip 15pt
        }}
\makeatother

% Définition des traits séparateurs du document
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}
\setlength{\columnseprule}{1pt}
\newcommand{\hsp}{\hspace{20pt}}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% Informations pour la page de garde
\date{\today}
\newcommand{\subjectID}[1]{\renewcommand{\subjectID}{#1}}
\newcommand{\subjectTitle}[1]{\renewcommand{\subjectTitle}{#1}}
\renewcommand{\title}[1]{\renewcommand{\title}{#1}}
\renewcommand{\author}[1]{\renewcommand{\author}{#1}}
\newcommand{\subtitle}[1]{\renewcommand{\subtitle}{#1}}
\newcommand{\professor}[1]{\renewcommand{\professor}{#1}}
\newcommand{\students}[1]{\renewcommand{\students}{#1}}
\newcommand{\associates}[1]{\renewcommand{\associates}{#1}}

\makeatletter

% En-têtes et pieds de page
\setlength{\headheight}{13pt}
\fancyheadoffset{1cm}
\setlength{\headheight}{2cm}

\fancypagestyle{fancy}{
    \fancyhf{}
    \fancyhead[LE,RO]{\includegraphics[width=0.2\textwidth]{logo.png}} %Affichage de l'image au top de la page
    \fancyhead[RE,LO]{\nouppercase{\leftmark}}
    \fancyfoot[LE,RO]{\thepage}
    \fancyfoot[RE,LO]{\subjectID}
    \fancyfoot[C]{\title}
}

\fancypagestyle{plain}{%
    \fancyhf{}
    \fancyfoot[LE,RO]{\thepage}
    \fancyfoot[RE,LO]{\subjectID}
    \fancyfoot[C]{\title}
    \renewcommand{\headrulewidth}{0pt}}

%% tableau
\RequirePackage{array}

%\newcolumntype{M}[1]{>{\raggedright}m{#1}}

%% enlever indentations
%\setlength{\parindent}{0pt}
%% taille entre les paragraphes
\setlength{\parskip}{1em}
%% taille entre les items 
\RequirePackage{enumitem}
\setlist{nosep}

\newcommand{\makeTitlePage}{
    \pagestyle{empty}
    \ThisLRCornerWallPaper{0.4}{titlePageBackground.png}
    \begin{titlepage}
        \centering

        \includegraphics[width=0.5\textwidth]{logo.png}~\\[2.5cm]

        {\Large \subjectID~: \subjectTitle }\\[1.3cm]

        % Titre
        \HRule \\[0.4cm]

        { \huge \bfseries \title \\}
        { \LARGE \subtitle \\[0.4cm]}

        \HRule \\[1cm]

        % Auteur et Enseignant
        \begin{minipage}{1\textwidth}
            \large
            \centering
            \associates
        \end{minipage}

        % Bottom of the page
        \vfill
        {\large \@date}

    \end{titlepage}
}

\newcommand{\makeTableOfContent}{
    \renewcommand{\contentsname}{\vspace{-3cm} \bfseries\Huge \tableOfContentName} % si tableofcontents au début
    \begingroup
    \renewcommand{\baselinestretch}{0.5}\normalsize
    \makeatletter% Allow use of restricted @ in control sequences
    % \let\ps@plain\ps@empty
    \setcounter{tocdepth}{1}
    \tableofcontents\thispagestyle{plain}
    \renewcommand{\baselinestretch}{1.0}\normalsize
    \endgroup
    \pagestyle{empty}

}

\newcommand{\makeAbstract}[2]{
    \pagestyle{plain}
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{fr}}}
    {\section*{Résumé}}{\section*{Abstract}}
    #2
}