# Modèle de rapport ENSEIRB-Matmeca LaTeX👋
![Version](https://img.shields.io/badge/version-0.9.0-blue.svg?cacheSeconds=2592000)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](README.md)
[![License: ICS](https://img.shields.io/badge/License-ICS-yellow.svg)](#)

> Ce dépôt contient le modèle LaTeX que j'utilise pour mes rapports. Il est actuellement dans ses premières versions, il va donc beaucoup évoluer ces prochains mois.

### 🏠 [Accueil](README.md)

# Structure du document
L'ensemble des fichiers nécessaires à la compilation sont organisés de la manière suivante :
```
├── assets
│   ├── img                         # Images du document
│   └── listings                    # Codes à lister
├── assets
├── src
│   ├── appendix.tex                # Annexes
│   ├── shortcuts.tex               # Liste de raccourcis utiles
│   ├── part1.tex                   # Exemple de partie
│   ├── reportENSEIRB-Matmeca.cls   # Classe de rapport type ENSEIRB-Matmeca
│   └── main.tex                    # Document principal
└── README.md
```
# Utilisation
## Extensions requises
La liste des paquets LaTeX utilisés par la class [Rapport ENSEIRB](/src/reportENSEIRB-Matmeca.cls) ainsi que dans le [document principal](/src/main.tex) se trouve dans ces fichiers respectifs.
## Compiler
La compilation du document (en tout cas dans sa version "out of the git")se fait aisément avec `latexmk` :
```sh
cd doc # Pour se rendre dans le dossier où se trouve le document
latexmk -pdf -c --shell-escape rapport.pdf
cd .. # Retour au dossier racine
```

On utilise les options suivantes:
- `-pdf` pour produire un fichier PDF ;
- `--shell-escape` pour les listings gérés par `minted` ;
- `-c` pour supprimer les fichiers intermédiaires de compilation (optionnel).

# Remerciements
J'aimerais remercier M. Pierre-Marie Princiaux, professeur de mathématiques en MPSI au lycée Lafayette à Clermont-Ferrand pour la liste des raccourcis utilisés dans `shortcuts.tex` et qui m'ont fait gagner et me font encore gagner énormément de temps dans la rédaction de mes documents dès qu'ils comportent un peu de mathématiques. Remerciements aussi aux 

# Auteur

👤 **Mathieu Dupoux**
* LinkedIn: [@mathieudupoux](https://linkedin.com/in/mathieudupoux)

---
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_